from __future__ import division
import numpy as np
import math
import sys
import cmath
import os
import random
import matplotlib.pyplot as plt
#from thermal import thermal

class Sim:
    def __init__(self, **kwargs):
        self.args = kwargs
        self.envReset(**kwargs)

    def envReset(self,
            lift = 6.5, # Initial lift, kg-force
            mass = 1.5+0.42+0.25+0.25+0.75+2.0, # Initial mass, kg
            v = 0, # Vertical velocity, meters per second
            h = 0, # Altitude, meters
            hmin = 0, # Minimum altitude, meters
            Vv = 1.0, # Velocity venting constant
            Vh = 1/1250., # Altitude venting constant
            Vt = 1/2000., # Last-vent-height constant
            Vsph = 14250, # Ballast setpoint height
            Vlvh = 0, # Last vent height, meters
            Bv = -1, # Velocity ballast constant
            Bh = 1/1250., # Altitude ballast constant
            Bt = 1/1500., # Last-ballast-height constant
            Bsph = 14000, # Ballast setpoint height, meters
            Bldh = -90000, # Ballast last drop height
            ventrate = 4.6/1000/2, #7.421/1000, #0.4/123.511/6., # Venting rate, lift units per second
            droprate = 0.83/1000., # 0.5/1000*1.2/1.6*1.01436 # Drop rate, kg per second
            Barm = -90000, # Ballast arming altitude, meters
            dt = 10, # Delta time for simulations, seconds
            T = 14*86400, # 200*3600, # Total simualtion time, seconds
            venttime = 15, # Vent time, seconds
            droptime = 10, # Drop time, seconds
            structural_mass = 470+1500+200, # 200 grams + balloon? lol?
            battery_height = 0.0, # battery height, inches
            reuse = True,
            silent = False,
            tdelta = 0.3
           ):

        self.delta = 0.3*86400
        self.ballast = 2.
        self.__dict__.update(locals())
        self.armed = False
        self.t = 0
        self.valveT = 0
        self.balT = 0
        self.times = np.arange(0, T, dt)
        self.lstfacts = 0*self.times+((1-0.05/(1+np.exp(-15*np.sin(2*np.pi*(self.times-self.delta)/24/3600)))))
        self.itn = -1
        self.errs = np.random.normal(0,0.05,int(T/dt)+1)
        while self.h < 13500: self.envStep(-1)
        #self.lift -= 1.0
        print "initial",self.state()
        return self.state()

    def state(self):
        return (self.h, self.v, self.ballast, (self.t-self.delta)%86400, self.Vlvh, self.Bldh)

    def envStep(self, action):
        self.itn += 1
        if self.itn == len(self.lstfacts)-1:
            return (self.state(), 0, True, {"no":"its"})

        self.h += self.v*self.dt
        if self.h > 11000:
            self.Barm = 11500
            self.Bldh = 13500

        if action != -1 and (self.h < 13000 or self.h > 17000):
            return (self.state(), -1000, True, {"shit":"happens"})

        if self.h >= self.Barm and self.Barm>0:
            self.armed = True
        if self.valveT <= 0 and (action == 1 or (action == -1 and (self.Vv*self.v+self.Vh*(self.h-self.Vsph)+self.Vt*(self.h-self.Vlvh)) > 1)):
            self.valveT = 10
        if self.valveT > 0:
            self.Vlvh = self.h
            self.lift -= self.ventrate*self.dt
            self.valveT -= self.dt
        if self.balT >= 0 and (action == 2 or (action == -1 and (self.Bv*self.v-self.Bh*(self.h-self.Bsph)-self.Bt*(self.h-self.Bldh)) > 1)): #and self.armed and self.ballast > 0:
            self.balT = self.droptime
            #if t > 0.26*86400 and t < 0.35*86400: totaltotal += droptime

        if self.balT > 0 and self.ballast > 0:
            self.Bldh = self.h
            self.mass -= self.droprate*self.dt
            self.ballast -= self.droprate*self.dt
            self.balT -= self.dt


        lft = lambda: self.lstfacts[self.itn]*self.lift
        f = lambda: lft()-self.mass
        def vel():
            #print self.itn, self.T, self.lstfacts
            fres = self.lstfacts[self.itn]*self.lift-self.mass
            return math.copysign(3*math.sqrt(abs(fres)),fres)

        self.v = vel()+self.errs[self.itn]

        if self.ballast < 0:
            return (self.state(), -1000, True, {"ballast":"nope"})
        
        self.t += self.dt#random.normalvariate(dt, 0.000001) 
        #return (self.state(), 1-min(((self.h-13000)/14000.)**2,((self.h-15000)/14000.)**2)-0.05*(action != 0), False, {})
        return (self.state(), 1-0.01*(action != 0)-0.05*abs(self.v), False, {}) # -(self.h>16000)*(1)-(self.h<13000)*(2)-0.05*(action != 0)-(self.h>13000 and self.h<16000)*abs(self.v/5.)*0.05-0*(action==2 and self.v > 0)*10 # 0.25+((self.h-15000)/14000.)**2

if __name__ == '__main__':
    s=Sim()
    print s.h
    h=[]
    for i in range(390):
        a = s.envStep(-1)
        if a[2] == True:
            print s.t, a
            break
        h.append(a[0])
    import matplotlib.pyplot as plt
    plt.plot(h)
    plt.show()
