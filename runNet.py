import numpy as np

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from keras.optimizers import Adam

from rl.agents.dqn import DQNAgent
from rl.policy import BoltzmannQPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory

from customEnv import flightSimulatorEnv

ENV_NAME = "valbal_simulated_v12"

actions = ["nothing_10","valve_10","ballast_10"]#, "ballast_15"]
observations = ["height", "height_velocity","mass_ballast","day_time","last_vent","last_ballast"] #, "sun_angle", "sun_angle_rate", 
#"h_m5", "h_m10", "h_m15", "a_r1", "a_r2", "a_r3"]

env = flightSimulatorEnv(actions, observations)

nb_actions = env.action_space.n

# Next, we build a very simple model.
model = Sequential()
model.add(Flatten(input_shape=(1,) + (env.observation_space.n,)))
model.add(Dense(16))
model.add(Activation('relu'))
#model.add(Dense(16))
#model.add(Activation('relu'))
model.add(Dense(16))
model.add(Activation('relu'))
model.add(Dense(nb_actions))
model.add(Activation('linear'))
print(model.summary())

# Finally, we configure and compile our agent. You can use every built-in Keras optimizer and
# even the metrics!
memory = SequentialMemory(limit=50000, window_length=1)
policy = BoltzmannQPolicy(tau=0.5)

from rl.policy import Policy
class AndreyPolicy(Policy):
 def __init__(self, env):
  self.env = env
 def select_action(self, q_values):
  sim = self.env.simulator
  self = sim
  if self.Bv*self.v-self.Bh*(self.h-self.Bsph)-self.Bt*(self.h-self.Bldh) > 1:
   return 2
  if self.Vv*self.v+self.Vh*(self.h-self.Vsph)+self.Vt*(self.h-self.Vlvh) > 1:
   return 1
  return 0
andrey = AndreyPolicy(env)
policy = EpsGreedyQPolicy(eps=0.001)
dqn = DQNAgent(model=model, nb_actions=nb_actions, memory=memory, nb_steps_warmup=0,
               target_model_update=1e-2, policy=policy)
dqn.compile(Adam(lr=1e-3), metrics=['mae'])

#dqn.load_weights('dqn_{}_weights.h5f'.format(ENV_NAME))
# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
# Ctrl + C.
#dqn.fit(env, nb_steps=500000, visualize=False, verbose=2)
#dqn.policy = policy
dqn.fit(env, nb_steps=500000000, visualize=False, verbose=2)

# After training is done, we save the final weights.
#ENV_NAME = "valbal_simulated_v3"
dqn.save_weights('dqn_{}_weights.h5f'.format(ENV_NAME), overwrite=True)

# Finally, evaluate our algorithm for 5 episodes.
dqn.test(env, nb_episodes=5, visualize=True)
