## create a custom valbal environment that keras-rl can train in
import random
from rl.core import Env, Space
from flightSimulator import Sim

class flightSimulatorSpace(Space):
    """Abstract model for a space that is used for the state and action spaces. This class has the
    exact same API that OpenAI Gym uses so that integrating with it is trivial.
    """
    def __init__(self, space_vars):
        self.space_vars = space_vars
        self.n = len(space_vars)

    def sample(self, seed=None):
        """Uniformly randomly sample a random element of this space.
        """
        return random.choice(self.space_vars)

    def contains(self, x):
        """Return boolean specifying if x is a valid member of this space
        """
        return x in self.space_vars


class flightSimulatorEnv(Env):
    metadata = {'render.modes': ['human']}
    def __init__(self, action_space_vars, observation_space_vars):
        self.action_space = flightSimulatorSpace(action_space_vars)
        self.observation_space = flightSimulatorSpace(observation_space_vars)
        self.simulator = Sim()
        self.times = []
        self.alts = []
        self.other = []
        self.original = False

    def render(self, mode='human', close=False):
        self.times.append(self.simulator.t)
        self.alts.append(self.simulator.h)
        self.other.append(self.simulator.ballast)

    def step(self, action):
        """Run one timestep of the environment's dynamics.
        Accepts an action and returns a tuple (observation, reward, done, info).
        Args:
            action (object): an action provided by the environment
        Returns:
            observation (object): agent's observation of the current environment
            reward (float) : amount of reward returned after previous action
            done (boolean): whether the episode has ended, in which case further step() calls will return undefined results
            info (dict): contains auxiliary diagnostic information (helpful for debugging, and sometimes learning)
        """
        self.simulator.original = self.original
        return self.simulator.envStep(action)

    def reset(self):
        """
        Resets the state of the environment and returns an initial observation.
        Returns:
            observation (object): the initial observation of the space. (Initial reward is assumed to be 0.)
        """
        if len(self.times) != 0:
            print "ballast", self.other[-1]
            import matplotlib.pyplot as plt
            plt.cla()
            plt.clf()
            plt.plot(self.times, self.alts)
            plt.show()
            self.times = []
            self.alts = []
        x = self.simulator.envReset()
        self.simulator.original = self.original
        return x

    def close(self):
        pass

